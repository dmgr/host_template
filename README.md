# Aconfmgr2 Host template

This is a basic template to create a new host from config.

## Usage

Dmgr automatically clone this repository when a new host configuration is created. When the `aconfgmr save myhost`is run, it will populate `files/` and `states/99-unsorted.sh`. Once saved, you just have to tidy the `99-unserted.sh` file onto other files matching name pattern: `states/[0-9][0-9]-STATE.sh`. You can also decide to remove the resource on the system if you are not happy to integrate this change in your conf. If you think you're good, deletes the files (`rm -rf files/ states/99-unsorted.sh`), and recheck the save output. If there is not difference, aka a `states/99-unsorted.sh` been recreated, commit all your changes and you are done.

## Dependencies
This template depends on:

* Base Archlinux: [base_arch](https://framagit.org/dmgr/base_archlinux), provides base.
* Dist JezArch: [dist_jezarch](https://framagit.org/dmgr/dist_archjez), provides user and aur support.
* Host Template: This repo, represent the state of one host.
