# Main config (Override)
SYS_HOSTNAME=myhost
SYS_DOMAIN=mydomain
SYS_ZONEINFO=Europe/Paris
SYS_LOCALE=en_US.UTF-8
SYS_EFI_PART=myefipart

# Import inherited config
Require dist_jezarch https://framagit.org/dmgr/dist_archjez.git
Import dist_jezarch lib

